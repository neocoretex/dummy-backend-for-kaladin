var express = require('express');
var app = express();
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bodyParser = require('body-parser');

var userschema = new Schema({
  username: {type:String,
        unique:true},
  password: String
});

var User = mongoose.model('User', userschema);


//CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//connect to database
mongoose.connect('mongodb://localhost:27017/mocklogindb');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function(){
  console.log('Database connected');
});

app.use(bodyParser.json({type:'application/*+json'}));
app.use(bodyParser.urlencoded({ extended: false }));

app.post('/users',function(req,res){
  var user = new User({username: req.body.data.attributes.username,password: req.body.data.attributes.password });
  user.save(function(err){
    if (err) {
      if(err.toJSON().code === 11000){
        res.status(422).json({errors:[{detail:'Username already taken'}]});
      }
      else{
        res.status(422).json({errors:[{detail:'Signup failed'}]});
      }
      
      return;
  }
    res.status(200).json({data:null});
  });
});

app.get('/users',function(req,res){
  mongoose.model('User').find({},function(err,users){
    if(err) {
      res.status(422).json({errors:[{detail:'Cant find users'}]});
      return;
    }
    users.forEach(function(user,i){
      var temp = users[i];
      users[i] = {_id:temp._id,
                  type:'user',
                  attributes:{
                    username:temp.username,
                    password:temp.password
                  }};
    })
    res.status(200).json({data:users});
  });
  
  
});

app.listen(3000,function(){
  console.log('Server at 3000');
})